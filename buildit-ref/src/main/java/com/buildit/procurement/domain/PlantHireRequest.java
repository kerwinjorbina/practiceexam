package com.buildit.procurement.domain;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Data
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)

public class PlantHireRequest {
    @Id @GeneratedValue
    Long id;

    @Column(precision = 8, scale = 2)
    BigDecimal total;

    @ElementCollection
    @AttributeOverrides({@AttributeOverride(name="id", column=@Column(name="plant_hire_request_items"))})
    List<PHRLineItem> phrLineItems;

    public static PlantHireRequest of() {
        PlantHireRequest phr = new PlantHireRequest();
        return phr;
    }
}
