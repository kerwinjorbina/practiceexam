package com.buildit.procurement.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PHRLineItemRepository extends JpaRepository<PHRLineItem, Long> {
    List<PHRLineItem> findAllByPlantHireRequest(Long id);
}
