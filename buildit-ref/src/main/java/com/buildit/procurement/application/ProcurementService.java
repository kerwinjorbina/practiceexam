package com.buildit.procurement.application;

import com.buildit.procurement.application.dto.PHRLineItemDTO;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.domain.*;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import com.buildit.rental.application.service.RentalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ProcurementService {
    @Autowired
    RentalService rentalService;
    @Autowired
    PlantHireRequestRepository plantHireRequestRepository;
    @Autowired
    PHRLineItemRepository phrLineItemRepository;
    @Autowired
    PHRLineItemAssembler assembler;
    @Autowired
    PlantHireRequestAssembler plantHireRequestAssembler;

    public PlantHireRequestDTO createPHR(PlantHireRequestDTO plantHireRequestDTO) throws Exception{
        PlantHireRequest phr = PlantHireRequest.of();

        phr = plantHireRequestRepository.save(phr);

        List<PHRLineItemDTO> items = plantHireRequestDTO.getItems();
        List<PHRLineItem> phrLineItems = new ArrayList<>();
        for(PHRLineItemDTO item : items){
            phrLineItems.add(createPHRItem(item, phr.getId()));
        }
        phr.setPhrLineItems(phrLineItems);

        return plantHireRequestAssembler.toResource(phr);
    }

    public PHRLineItem createPHRItem(PHRLineItemDTO phrDTO, Long phrId) throws Exception {
        PHRLineItem phrLineItem = PHRLineItem.of(
                PlantInventoryEntry.of(phrDTO.getPlant().getLink("self").getHref(), phrDTO.getPlant().getName()),
                BusinessPeriod.of(phrDTO.getRentalPeriod().getStartDate(), phrDTO.getRentalPeriod().getEndDate()),
                phrId
        );
        phrLineItemRepository.save(phrLineItem);

        PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
        poDTO.setPlant(phrDTO.getPlant());
        poDTO.setRentalPeriod(phrDTO.getRentalPeriod());
        poDTO = rentalService.createPurchaseOrder(poDTO);

        phrLineItem.updatePurchaseOrder(PurchaseOrder.of(poDTO.getLink("self").getHref(), poDTO.getTotal(), poDTO.getStatus()));

        phrLineItemRepository.save(phrLineItem);

        return phrLineItem;
    }

    public PHRLineItemDTO closePHR(Long id) {
        PHRLineItem phrLineItem = phrLineItemRepository.findOne(id);
        phrLineItem.closePHR();
        phrLineItemRepository.save(phrLineItem);

        return assembler.toResource(phrLineItem);
    }

    public List<PHRLineItemDTO> getAllPHR() {
        return assembler.toResources(phrLineItemRepository.findAll());
    }

    public PlantHireRequestDTO findPHR(Long id) {
        return plantHireRequestAssembler.toResource(plantHireRequestRepository.getOne(id));
    }

    public PHRLineItemDTO updatePHRPOStatus(Long id) throws Exception {
        PHRLineItem phrLineItem = phrLineItemRepository.findOne(id);
        PurchaseOrderDTO poDTO = new PurchaseOrderDTO();
        poDTO.add(new Link(phrLineItem.getPurchaseOrder().getHref()));
        poDTO = rentalService.fetchPurchaseOrder(poDTO);
        phrLineItem.updatePurchaseOrder(PurchaseOrder.of(poDTO.getLink("self").getHref(), poDTO.getTotal(), poDTO.getStatus()));
        phrLineItemRepository.save(phrLineItem);
        return assembler.toResource(phrLineItem);
    }
}
