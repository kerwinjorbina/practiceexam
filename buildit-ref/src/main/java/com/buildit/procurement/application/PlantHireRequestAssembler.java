package com.buildit.procurement.application;


import com.buildit.common.rest.ExtendedLink;
import com.buildit.procurement.application.dto.BusinessPeriodDTO;
import com.buildit.procurement.application.dto.PHRLineItemDTO;
import com.buildit.procurement.application.dto.PlantHireRequestDTO;
import com.buildit.procurement.domain.PHRLineItem;
import com.buildit.procurement.domain.PHRLineItemRepository;
import com.buildit.procurement.domain.PlantHireRequest;
import com.buildit.procurement.domain.PlantHireRequestRepository;
import com.buildit.procurement.rest.PlantHireRequestController;
import com.buildit.rental.application.dto.PurchaseOrderDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.hateoas.mvc.ResourceAssemblerSupport;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;
import static org.springframework.http.HttpMethod.DELETE;
import static org.springframework.http.HttpMethod.PATCH;

@Service
public class PlantHireRequestAssembler extends ResourceAssemblerSupport<PlantHireRequest, PlantHireRequestDTO> {

    @Autowired
    PHRLineItemAssembler phrLineItemAssembler;

    @Autowired
    PHRLineItemRepository repository;

    public PlantHireRequestAssembler() {
        super(PlantHireRequestController.class, PlantHireRequestDTO.class);
    }

    public PlantHireRequestDTO toResource(PlantHireRequest phr) {
        PlantHireRequestDTO dto = createResourceWithId(phr.getId(), phr);

        List<PHRLineItem> items = repository.findAllByPlantHireRequest(phr.getId());
        List<PHRLineItemDTO> phrLineItemDTO = new ArrayList<>();
        for(PHRLineItem item : items){
            phrLineItemDTO.add(phrLineItemAssembler.toResource(item));
        }

        dto.setItems(phrLineItemDTO);
        dto.setTotal(phr.getTotal());

        return dto;
    }
}
