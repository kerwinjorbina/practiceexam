package com.buildit.procurement.domain;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;

@Embeddable
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PROTECTED)
public class PlantInventoryEntry {

    String href;
    String name;

    public static PlantInventoryEntry of(String href, String name) {
        PlantInventoryEntry plantInventoryEntry = new PlantInventoryEntry();
        plantInventoryEntry.href = href;
        plantInventoryEntry.name = name;
        return plantInventoryEntry;
    }
}
